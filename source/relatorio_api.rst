Relatório API
==================================================

`Foi criado uma API Rest para visualização dos arquivos exportados`

Visualização da documentação API
---------------------------------
Para ver os endpoints criados, com a aplicação executada, acesse

Redoc
``http://0.0.0.0:8000/docs/redoc/``

Swagger
``http://0.0.0.0:8000/docs/swagger/``

.. image:: images/photo5.png

Redoc


Visualização pelo django admin
-------------------------------
Acesse

Django Admin
``http://0.0.0.0:8000/admin/``

.. image:: images/admin.png


Acesso::


	user: admin
	password: admin2020

