Relatório e Execução
==================================================

`Executando manualmente a task e lendo relatórios`

Execução de task e retorno de relatório manualmente
----------------------------------------------------

Você pode também executar as tasks de importação e exportação manualmente, vejamos:

**Importacao de novos arquivos**

``sudo docker exec -it project python.manage.py analyze run``


Você verá uma tela como essa:
.. image:: images/photo1.png

Relatórios
--------------------------

**Listar todos os relatorios existentes**

``sudo docker exec -it project python.manage.py reports list``

**Resultado**
    .. image:: images/photo2.png

**Todos os clientes**

``sudo docker exec -it project python.manage.py reports clients 1``

**Resultado**
    .. image:: images/photo3.png


**Todos os Vendedores**

``sudo docker exec -it project python.manage.py reports sellers 1``

**Resultado**
    .. image:: images/photo4.png

**Melhor Venda**

``sudo docker exec -it project python.manage.py reports best_sale 1``

**Resultado**
    .. image:: images/photo5.png

**Pior vendedor**

``sudo docker exec -it project python.manage.py reports worse_saller 1``

**Resultado**
    .. image:: images/photo6.png

