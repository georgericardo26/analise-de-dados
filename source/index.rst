Análise de dados - Documentação Oficial
==================================================

Bem vindo a documentação do projeto de leitura de dados.

Overview
-----------

.. toctree::
   :maxdepth: 6

   introducao
   instalacao
   relatorio_cli
   relatorio_api
   conclusao
   contents
