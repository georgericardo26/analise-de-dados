Introdução
==================================================


`Este projeto foi criado com o intuito de permitir ao usuário, executá-lo localmente utilizando Django-CLI e API Rest`


Necessidade
--------------------------
Criar uma aplicação que verifica novos arquivos sendo inseridos no diretório /data/in/, tratá-los e exportá-los cara o
diretório data/out/

* Problemática
   * Problema 1 : Como a aplicação será executada localmente utilizando tarefas periódicas, será necessário utilizar o crontrab do servidor local
   * Problema 2: crontab so funciona em ambientes linux, então se alguém executasse localmente em um ambiente windows não funcionaria a verificação periódica.
   * Problema 3: Mesmo usando um docker, as imagens do docker geralmente vem bem limitadas e com nenhuma pre-configuração para funcionalidades periódicas.

* Solucão: Criei um docker utilizando a imagem ubuntu:16.4 (Sim testei com o centos, amazonlinux e o Linux Nativo do docker), mas o ubuntu foi quem conseguiu satisfazer visto que não seria somente criar a imagem, mas instalar o docker, um supervisor, parametrizar com cron(sim, o container não vem nada), e ainda setar para que essas crons somente executassem como root (geralmente crontab executa como usuários normais, mas uma imagem docker nao cria usuários).


Estrutura do projeto
--------------------------
Bom o projeto utiliza de 3 interfaces:
	* 1- CLI: Para executar todas as requisições do projeto através de linha de comando. (importar arquivo, gerar os relatórios)
	* 2- Django Admin: Para acessar todos os arquivos que foram exportados.
	* 3- API Rest: Para gerar o link apontando para os arquivos exportados e seus relatórios (como JSON Object).
A estrutura de arquivos possui 4 diretórios principais
	:/insert: que faz toda a importação, exportação, e geração de relatórios
	:/data: diretoria com os arquivos a serem importados e os arquivos exportados
	:/data_analyze: diretório base do projeto django
	:/web: app django para construir a API usando django rest framework


.. _my-reference-label:

Ferramentas utilizadas
--------------------------
As ferramentas utilizadas foram::

	Docker
	Docker compose
	Postgres
	Redis
	Python 3
	Django
	Django Rest Framework
	Celery
	Swagger
	Redoc
    Bitbucket (Repositorio)
    Jira (Sim, eu só consigo me auto gerenciar usando scrum)

