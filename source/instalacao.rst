Instalação
==================================================

`Instalando o projeto localmente`

Para instalar o projeto, primeiro é preciso primeiro instalar o seguinte:


* Docker
* Docker compose

Faça um clone do repositório para seu diretorio.

``git clone https://georgericardo26@bitbucket.org/georgericardo26/data-analyze.git``

``cd data-analyze``

``python manage.py runserver``


Pronto, agora quando voce quiser, você pode adicionar novos arquivos .txt no diretorio
``/data/in/`` com o padrão::
001ç1234567891234çPedroç50000
001ç3245678865434çPauloç40000.99
002ç2345675434544345çJose da SilvaçRural
002ç2345675433444345çEduardo PereiraçRural
003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro
003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo
003ç09ç[1-34-10,2-33-1.50,3-40-0.10]çRicardo
003ç11ç[1-34-10,2-33-1.50,3-40-0.10]çRicardo


** Na próxima seção eu explico como executar a importação manualmente e ler os relatórios
